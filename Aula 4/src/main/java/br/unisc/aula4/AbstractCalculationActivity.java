package br.unisc.aula4;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import br.unisc.aula4.dialog.ResultDialog;

public abstract class AbstractCalculationActivity extends Activity {

    protected abstract Bundle calculate();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calculation, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        if (item.getItemId() == R.id.menuitem_calculate) {
            try {
                Bundle args = this.calculate();
                DialogFragment dialog = new ResultDialog();
                dialog.setArguments(args);
                dialog.show(this.getFragmentManager(), "result");
            } catch (Exception e) {}
        }

        return super.onMenuItemSelected(featureId, item);
    }
}
