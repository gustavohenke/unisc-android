package br.unisc.aula4.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ResultDialog extends DialogFragment {

    public static final String TITLE = "title";
    public static final String TEXT = "text";

    // http://developer.android.com/guide/topics/ui/dialogs.html
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = this.getArguments();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(args.getString(TITLE));
        builder.setMessage(args.getString(TEXT));

        return builder.create();
    }
}