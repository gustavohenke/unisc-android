package br.unisc.aula4;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import br.unisc.aula4.dialog.ResultDialog;

public class WeightActivity extends AbstractCalculationActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.weight);
    }

    @Override
    protected Bundle calculate() {
        EditText inputName = (EditText) findViewById(R.id.input_weight_name);
        String name = inputName.getText().toString();
        name = name.equals("") ? "Sem nome" : name;

        NumberFormat formatter = new DecimalFormat("0.0");
        double peso = this.getPesoIdeal();
        String msg = name + ", seu peso ideal é " + formatter.format(peso) + " kg";

        Bundle args = new Bundle();
        args.putString(ResultDialog.TITLE, getString(R.string.menu_weight));
        args.putString(ResultDialog.TEXT, msg);
        return args;
    }

    private double getPesoIdeal() {
        EditText inputHeight = (EditText) findViewById(R.id.input_weight_height);
        float height = Float.valueOf(inputHeight.getText().toString());

        Spinner inputGender = (Spinner) findViewById(R.id.input_weight_gender);
        String gender = (String) inputGender.getSelectedItem();

        if (gender.equals(getString(R.string.gender_male))) {
            return (72.7 * height) - 58;
        } else {
            return (62.1 * height) - 44.7;
        }
    }
}