package br.unisc.aula4;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import br.unisc.aula4.dialog.ResultDialog;

public class FactorialActivity extends AbstractCalculationActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.factorial );

        Spinner spinner = ( Spinner ) findViewById( R.id.input_factorial_number );

        ArrayAdapter< CharSequence > adapter = ArrayAdapter.createFromResource(
                this,
                R.array.factorial_options,
                android.R.layout.simple_spinner_item
        );

        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        spinner.setAdapter( adapter );
    }

    @Override
    protected Bundle calculate() {
        Spinner inputNumber = (Spinner) findViewById(R.id.input_factorial_number);
        String number = (String) inputNumber.getSelectedItem();

        long num = Long.valueOf(number);
        String msg = "O fatorial de " + num + " é " + factorial(num);

        Bundle args = new Bundle();
        args.putString(ResultDialog.TITLE, getString(R.string.menu_factorial));
        args.putString( ResultDialog.TEXT, msg);

        return args;
    }

    private long factorial (long n) {
        long val = 1;
        while (n > 1) {
            val *= n;
            n--;
        }

        return val;
    }
}