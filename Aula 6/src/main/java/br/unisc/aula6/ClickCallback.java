package br.unisc.aula6;

import android.app.Fragment;

public interface ClickCallback {

    void onClick ( Fragment origem, String message );

}
