package br.unisc.aula6;

import android.app.Activity;
import android.app.Fragment;
import android.view.View;

public abstract class BaseFragment extends Fragment implements View.OnClickListener {

    public static final String MESSAGE_KEY = "mensagem";

    private ClickCallback callback;

    public abstract String getMessage ();
    public abstract void update ();

    @Override
    public void onAttach ( Activity activity ) {
        super.onAttach( activity );

        try {
            this.callback = ( ClickCallback ) activity;
        } catch ( Exception e ) {
        }
    }

    @Override
    public void onClick ( View v ) {
        this.callback.onClick( this, this.getMessage() );
    }
}
