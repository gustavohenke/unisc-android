package br.unisc.aula6;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class MainActivity extends Activity implements ClickCallback {

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        if ( this.getFragmentContainer() != null ) {
            this.addFragment( new EsquerdoFragment() );
        }
    }

    public void actEsquerdo ( View view ) {
        this.addFragment( new EsquerdoFragment() );
    }

    public void actDireito ( View view ) {
        this.addFragment( new DireitoFragment() );
    }

    private void addFragment ( Fragment fragment ) {
        getFragmentManager().beginTransaction()
                .replace( R.id.fragment_container, fragment )
                .commit();
    }

    private FrameLayout getFragmentContainer () {
        return ( FrameLayout ) findViewById( R.id.fragment_container );
    }

    @Override
    public void onClick ( Fragment origem, String message ) {
        BaseFragment fragment;
        boolean isEsquerdo = origem instanceof EsquerdoFragment;

        if ( this.getFragmentContainer() != null ) {
            if ( isEsquerdo ) {
                fragment = new DireitoFragment();
            } else {
                fragment = new EsquerdoFragment();
            }

            Bundle args = new Bundle();
            args.putString( BaseFragment.MESSAGE_KEY, message );
            fragment.setArguments( args );

            this.addFragment( fragment );
        } else {
            fragment = ( BaseFragment ) getFragmentManager().findFragmentById( isEsquerdo ? R.id.direito_fragment : R.id.esquerdo_fragment );
            fragment.getArguments().putString( BaseFragment.MESSAGE_KEY, message );
            fragment.update();
        }
    }
}
