package br.unisc.aula6;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class EsquerdoFragment extends BaseFragment {

    public EsquerdoFragment () {
        // Required empty public constructor
    }


    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState ) {
        View view = inflater.inflate( R.layout.fragment_esquerdo, container, false );

        Button button = ( Button ) view.findViewById( R.id.button_esquerdo );
        button.setOnClickListener( this );

        return view;
    }

    @Override
    public void onViewCreated ( View view, Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );
        this.update();
    }

    @Override
    public void update () {
        TextView receiver = ( TextView ) getView().findViewById( R.id.message_received_esquerdo );
        Bundle args = this.getArguments();

        if ( args != null ) {
            receiver.setText( args.getString( BaseFragment.MESSAGE_KEY ) );
        }
    }

    @Override
    public String getMessage () {
        return "Mensagem do fragment Esquerdo!";
    }
}
