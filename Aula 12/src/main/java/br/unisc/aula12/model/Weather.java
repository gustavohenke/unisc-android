package br.unisc.aula12.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

public class Weather {

    private int id;
    private String title;
    private String description;
    private String icon;
    private double temperature;
    private double pressure;
    private double humidity;
    private double wind;
    private Date date;

    public int getId () {
        return id;
    }

    public void setId ( int id ) {
        this.id = id;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription ( String description ) {
        this.description = description;
    }

    public double getHumidity () {
        return humidity;
    }

    public void setHumidity ( double humidity ) {
        this.humidity = humidity;
    }

    public String getIcon () {
        return icon;
    }

    public void setIcon ( String icon ) {
        this.icon = icon;
    }

    public double getPressure () {
        return pressure;
    }

    public void setPressure ( double pressure ) {
        this.pressure = pressure;
    }

    public double getTemperature () {
        return temperature;
    }

    public void setTemperature ( double temperature ) {
        this.temperature = temperature;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle ( String title ) {
        this.title = title;
    }

    public double getWind () {
        return wind;
    }

    public void setWind ( double wind ) {
        this.wind = wind;
    }

    public Date getDate () {
        return date;
    }

    public void setDate ( Date date ) {
        this.date = date;
    }

    public static Weather fromJSON ( JSONObject json ) {
        try {
            JSONObject objWeather = json.getJSONArray( "weather" ).getJSONObject( 0 );
            JSONObject objMain = json.getJSONObject( "main" );
            JSONObject objWind = json.getJSONObject( "wind" );

            Weather weather = new Weather();

            weather.setTitle( objWeather.getString( "main" ) );
            weather.setDescription( objWeather.getString( "description" ) );
            weather.setIcon( objWeather.getString( "icon" ) );
            weather.setTemperature( objMain.getDouble( "temp" ) );
            weather.setHumidity( objMain.getDouble( "humidity" ) );
            weather.setPressure( objMain.getDouble( "pressure" ) );
            weather.setWind( objWind.getDouble( "speed" ) );
            weather.setDate( new Date( json.getLong( "dt" ) * 1000 ) );

            return weather;
        } catch ( JSONException e ) {
            return null;
        }
    }

}
