package br.unisc.aula12.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;

import br.unisc.aula12.database.Database;
import br.unisc.aula12.database.DatabaseFactory;
import br.unisc.aula12.database.Types;

public class DatabaseDAO {

    private final Database database = new Database( new DatabaseFactoryImpl() );

    public void create ( Weather weather ) {
        ContentValues values = new ContentValues();
        values.put( Contract.TITLE, weather.getTitle() );
        values.put( Contract.DESCRIPTION, weather.getDescription() );
        values.put( Contract.ICON, weather.getIcon() );
        values.put( Contract.TEMPERATURE, weather.getTemperature() );
        values.put( Contract.HUMIDITY, weather.getHumidity() );
        values.put( Contract.PRESSURE, weather.getPressure() );
        values.put( Contract.WIND, weather.getWind() );
        values.put( Contract.DATE, new Date( weather.getDate().getTime() ).toString() );

        SQLiteDatabase db = database.getWritableDatabase();
        db.insert( Contract.TABLE, null, values );
    }

    public List< Weather > list () {
        SQLiteDatabase db = database.getReadableDatabase();
        Cursor cursor = db.query( Contract.TABLE, new String[]{
                Contract._ID,
                Contract.TITLE,
                Contract.DESCRIPTION,
                Contract.ICON,
                Contract.TEMPERATURE,
                Contract.HUMIDITY,
                Contract.PRESSURE,
                Contract.WIND,
                Contract.DATE
        }, null, null, null, null, Contract.DATE + " DESC" );

        List< Weather > weathers = new LinkedList<>();
        if ( cursor.getCount() > 0 ) {
            while ( cursor.moveToNext() ) {
                Weather weather = new Weather();
                weather.setId( cursor.getInt( cursor.getColumnIndex( Contract._ID ) ) );
                weather.setTitle( cursor.getString( cursor.getColumnIndex( Contract.TITLE ) ) );
                weather.setDescription( cursor.getString( cursor.getColumnIndex( Contract.DESCRIPTION ) ) );
                weather.setIcon( cursor.getString( cursor.getColumnIndex( Contract.ICON ) ) );
                weather.setTemperature( cursor.getDouble( cursor.getColumnIndex( Contract.TEMPERATURE ) ) );
                weather.setPressure( cursor.getDouble( cursor.getColumnIndex( Contract.PRESSURE ) ) );
                weather.setHumidity( cursor.getDouble( cursor.getColumnIndex( Contract.HUMIDITY ) ) );
                weather.setWind( cursor.getDouble( cursor.getColumnIndex( Contract.WIND ) ) );
                weather.setDate( Date.valueOf( cursor.getString( cursor.getColumnIndex( Contract.DATE ) ) ) );

                weathers.add( weather );
            }
        }

        cursor.close();
        return weathers;
    }

    // ---------------------------------------------------------------------------------------------

    private static class DatabaseFactoryImpl implements DatabaseFactory {
        private final static String NAME = "weather.db";
        private final static int VERSION = 1;

        @Override
        public String createDatabaseName () {
            return NAME;
        }

        @Override
        public int createDatabaseVersion () {
            return VERSION;
        }

        @Override
        public String createCreateStatement () {
            return "CREATE TABLE " + Contract.TABLE + " (" +
                    Contract._ID + Contract._ID_TYPE + " PRIMARY KEY AUTOINCREMENT," +
                    Contract.TITLE + Contract.TITLE_TYPE + ", " +
                    Contract.DESCRIPTION + Contract.DESCRIPTION_TYPE + ", " +
                    Contract.ICON + Contract.ICON_TYPE + ", " +
                    Contract.TEMPERATURE + Contract.TEMPERATURE_TYPE + ", " +
                    Contract.HUMIDITY + Contract.HUMIDITY_TYPE + ", " +
                    Contract.PRESSURE + Contract.PRESSURE_TYPE + ", " +
                    Contract.WIND + Contract.WIND_TYPE + ", " +
                    Contract.DATE + Contract.DATE_TYPE +
                    ");";
        }

        @Override
        public String createUpgradeStatement () {
            return "DROP TABLE " + Contract.TABLE + ";" + createCreateStatement();
        }
    }

    // ---------------------------------------------------------------------------------------------

    interface Contract extends BaseColumns {
        String TABLE = "weather";

        String _ID_TYPE = Types.INT;

        String TITLE = "title";
        String TITLE_TYPE = Types.STRING;

        String DESCRIPTION = "description";
        String DESCRIPTION_TYPE = Types.STRING;

        String ICON = "icon";
        String ICON_TYPE = Types.STRING;

        String TEMPERATURE = "temperature";
        String TEMPERATURE_TYPE = Types.DOUBLE;

        String HUMIDITY = "humidity";
        String HUMIDITY_TYPE = Types.DOUBLE;

        String PRESSURE = "pressure";
        String PRESSURE_TYPE = Types.DOUBLE;

        String WIND = "wind";
        String WIND_TYPE = Types.DOUBLE;

        String DATE = "date";
        String DATE_TYPE = Types.DATE;
    }

}