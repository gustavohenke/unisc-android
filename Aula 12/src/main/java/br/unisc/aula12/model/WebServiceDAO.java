package br.unisc.aula12.model;

import android.net.Uri;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class WebServiceDAO {

    private final static String TAG = "WS";
    private final static String API_SCHEME = "http";
    private final static String API_URL = "api.openweathermap.org";
    private final static String API_PATH = "/data/2.5";
    private final static String APP_ID = "e6885390566897ec78a3d86740ed3b7d";
    private final static String LANG = "pt";
    private final static String UNITS = "metric";
    private final static int CITY_ID = 3452925;
    private final OkHttpClient client = new OkHttpClient();

    public Weather getCurrentForecast () throws IOException {
        String url = getUrlBuilder()
                .appendPath( "weather" )
                .appendQueryParameter( "id", CITY_ID + "" )
                .build()
                .toString();

        Log.d( TAG, "Obtendo previsao..." );
        Request request = new Request.Builder().url( url ).get().build();
        Response response = client.newCall( request ).execute();

        try {
            JSONObject json = new JSONObject( response.body().string() );
            return Weather.fromJSON( json );
        } catch ( JSONException e ) {
            return null;
        }
    }

    private Uri.Builder getUrlBuilder () {
        return new Uri.Builder()
                .scheme( API_SCHEME )
                .authority( API_URL )
                .path( API_PATH )
                .appendQueryParameter( "APPID", APP_ID )
                .appendQueryParameter( "lang", LANG )
                .appendQueryParameter( "units", UNITS );
    }

}
