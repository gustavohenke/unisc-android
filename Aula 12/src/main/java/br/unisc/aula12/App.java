package br.unisc.aula12;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import br.unisc.aula12.receiver.ConnectionReceiver;

public class App extends Application {

    private static Context context;

    @Override
    public void onCreate () {
        super.onCreate();
        context = this;

        // Não sei de outra forma pra inicializar junto com o app.
        Intent intent = new Intent( ConnectionReceiver.BROADCAST );
        sendBroadcast( intent );
    }

    public static Context getContext () {
        return context;
    }
}
