package br.unisc.aula12;

import android.app.ListActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Observable;
import java.util.Observer;

import br.unisc.aula12.adapter.WeatherAdapter;
import br.unisc.aula12.model.DatabaseDAO;
import br.unisc.aula12.service.WeatherIntentService;

public class MainActivity extends ListActivity implements Observer {

    private WeatherAdapter adapter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        adapter = new WeatherAdapter( this );
        setListAdapter( adapter );
    }

    @Override
    protected void onResume () {
        super.onResume();
        resetAdapter();
        WeatherIntentService.eventBus.addObserver( this );
    }

    @Override
    protected void onPause () {
        super.onPause();
        WeatherIntentService.eventBus.deleteObserver( this );
    }

    private void resetAdapter () {
        Log.d( "activity", "resetando adapter" );

        adapter.clear();
        adapter.addAll( new DatabaseDAO().list() );
        adapter.notifyDataSetChanged();
    }

    @Override
    public void update ( Observable observable, Object data ) {
        runOnUiThread( new Runnable() {
            @Override
            public void run () {
                resetAdapter();
            }
        });
    }
}
