package br.unisc.aula12.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import java.io.IOException;
import java.util.Observable;

import br.unisc.aula12.model.DatabaseDAO;
import br.unisc.aula12.model.Weather;
import br.unisc.aula12.model.WebServiceDAO;

public class WeatherIntentService extends IntentService {

    public static final Observable eventBus = new EventBus();

    public static void start ( Context context ) {
        Intent intent = new Intent( context, WeatherIntentService.class );
        context.startService( intent );
    }

    public WeatherIntentService () {
        super( "WeatherIntentService" );
    }

    @Override
    protected void onHandleIntent ( Intent intent ) {
        try {
            Weather weather = new WebServiceDAO().getCurrentForecast();
            new DatabaseDAO().create( weather );
            ( ( EventBus ) eventBus ).setChanged();
            eventBus.notifyObservers();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    static class EventBus extends Observable {
        @Override
        public void setChanged () {
            super.setChanged();
        }
    }
}
