package br.unisc.aula12.database;

public interface Types {

    String STRING = " TEXT";
    String INT = " INTEGER";
    String DOUBLE = " DOUBLE";
    String DATE = " DATETIME";

}
