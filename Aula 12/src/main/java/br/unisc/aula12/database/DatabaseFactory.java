package br.unisc.aula12.database;

public interface DatabaseFactory {

    String createDatabaseName();
    int createDatabaseVersion();
    String createCreateStatement();
    String createUpgradeStatement();

}
