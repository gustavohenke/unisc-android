package br.unisc.aula12.database;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.unisc.aula12.App;

public class Database extends SQLiteOpenHelper {

    private final DatabaseFactory factory;

    public Database ( DatabaseFactory factory ) {
        super( App.getContext(), factory.createDatabaseName(), null, factory.createDatabaseVersion() );
        this.factory = factory;
    }

    @Override
    public void onCreate ( SQLiteDatabase db ) {
        db.execSQL( factory.createCreateStatement() );
    }

    @Override
    public void onUpgrade ( SQLiteDatabase db, int oldVersion, int newVersion ) {
        db.execSQL( factory.createUpgradeStatement() );
    }
}
