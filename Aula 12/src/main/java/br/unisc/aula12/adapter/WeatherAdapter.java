package br.unisc.aula12.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.NumberFormat;

import br.unisc.aula12.R;
import br.unisc.aula12.model.Weather;

public class WeatherAdapter extends ArrayAdapter< Weather > {

    public WeatherAdapter ( Context context ) {
        super( context, 0 );
    }

    @Override
    public View getView ( int position, View convertView, ViewGroup parent ) {
        if ( convertView == null ) {
            convertView = LayoutInflater.from( getContext() ).inflate( R.layout.item_weather, parent, false );
        }

        Weather weather = getItem( position );
        TextView textTitle = ( TextView ) convertView.findViewById( R.id.title );
        TextView textDescription = ( TextView ) convertView.findViewById( R.id.description );
        TextView textDate = ( TextView ) convertView.findViewById( R.id.date );
        TextView textTemperature = ( TextView ) convertView.findViewById( R.id.temperature );
        TextView textPressure = ( TextView ) convertView.findViewById( R.id.pressure );
        TextView textWind = ( TextView ) convertView.findViewById( R.id.wind );
        TextView textHumidity = ( TextView ) convertView.findViewById( R.id.humidity );

        textTitle.setText( weather.getTitle() );
        textDescription.setText( weather.getDescription() );

        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat( getContext() );
        DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat( getContext() );
        textDate.setText(
                dateFormat.format( weather.getDate() ) + " " +
                timeFormat.format( weather.getDate() )
        );

        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        textTemperature.setText( getContext().getString( R.string.temperature, numberFormat.format( weather.getTemperature() ) ) );
        textWind.setText( getContext().getString( R.string.wind, numberFormat.format( weather.getWind() ) ) );
        textPressure.setText( numberFormat.format( weather.getPressure() ) );
        textHumidity.setText( numberFormat.format( weather.getHumidity() ) );

        return convertView;
    }
}
