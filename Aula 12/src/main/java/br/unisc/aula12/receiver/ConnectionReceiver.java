package br.unisc.aula12.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class ConnectionReceiver extends BroadcastReceiver {
    private final static String TAG = "ConnectionReceiver";
    public final static String BROADCAST = "aula12.broadcast.CONNECTION";

    // Para produção; a cada 15 min
    private final static long INTERVAL = AlarmManager.INTERVAL_FIFTEEN_MINUTES;

    // Para testes; a cada 30 seg
//    private final static long INTERVAL = 1 * 30 * 1000;

    public ConnectionReceiver () {}

    @Override
    public void onReceive ( Context context, Intent intent ) {
        Intent weatherIntent = new Intent( context, WeatherReceiver.class );
        PendingIntent alarmIntent = PendingIntent.getBroadcast( context, 0, weatherIntent, 0 );

        ConnectivityManager connManager = ( ConnectivityManager ) context.getSystemService( Context.CONNECTIVITY_SERVICE );
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();

        AlarmManager alarmManager = ( AlarmManager ) context.getSystemService( Context.ALARM_SERVICE );

        if ( networkInfo != null && networkInfo.isConnected() ) {
            Log.d( TAG, "definindo alarme" );
            alarmManager.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP,
                    INTERVAL,
                    INTERVAL,
                    alarmIntent
            );
        } else {
            Log.d( TAG, "cancelando alarme" );
            alarmManager.cancel( alarmIntent );
        }
    }
}
