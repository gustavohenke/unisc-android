package br.unisc.aula12.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import br.unisc.aula12.service.WeatherIntentService;

public class WeatherReceiver extends BroadcastReceiver {
    public WeatherReceiver () {}

    @Override
    public void onReceive ( Context context, Intent intent ) {
        WeatherIntentService.start( context );
    }
}
