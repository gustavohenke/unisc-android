package br.unisc.aula5;

import android.app.ListActivity;
import android.os.Bundle;

public class MainActivity extends ListActivity {

    private static final Integer[] photos = new Integer[] {
        R.drawable.kitten1,
        R.drawable.kitten2,
        R.drawable.kitten3,
        R.drawable.kitten1,
        R.drawable.kitten2,
        R.drawable.kitten3
    };

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        KittenAdapter adapter = new KittenAdapter( this, photos );
        this.setListAdapter( adapter );
    }
}
