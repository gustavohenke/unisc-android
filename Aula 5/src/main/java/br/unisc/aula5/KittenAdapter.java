package br.unisc.aula5;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class KittenAdapter extends ArrayAdapter< Integer > {

    public KittenAdapter ( Context context, Integer[] kittens ) {
        super( context, 0, kittens );
    }

    @Override
    public View getView ( int position, View convertView, ViewGroup parent ) {
        int kittenId = getItem( position );

        if ( convertView == null ) {
            convertView = LayoutInflater.from( this.getContext() ).inflate( R.layout.listitem_main, parent, false );
        }

        ImageView photo = ( ImageView ) convertView.findViewById( R.id.photo_kitten );
        photo.setImageDrawable( getContext().getResources().getDrawable( kittenId ) );

        return convertView;
    }
}
