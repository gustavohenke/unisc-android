package br.unisc.aula8;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import br.unisc.aula8.db.Database;
import br.unisc.aula8.db.Pessoa;

public abstract class FormActivity extends Activity {

    private Pessoa pessoa;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_add );
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater().inflate( R.menu.menu_form, menu );
        return true;
    }

    protected final void setPessoa ( Pessoa pessoa ) {
        EditText inputNome = ( EditText ) findViewById( R.id.input_form_nome );
        EditText inputPhone = ( EditText ) findViewById( R.id.input_form_phone );
        EditText inputEmail = ( EditText ) findViewById( R.id.input_form_email );
        RadioButton inputGender = convertGenderToRadio( pessoa.getSexo() );

        inputNome.setText( pessoa.getNome() );
        inputEmail.setText( pessoa.getEmail() );
        inputPhone.setText( pessoa.getTelefone() );
        inputGender.setChecked( true );

        this.pessoa = pessoa;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        if ( item.getItemId() != R.id.menuitem_done ) {
            return super.onOptionsItemSelected( item );
        }

        final EditText inputNome = ( EditText ) findViewById( R.id.input_form_nome );
        final EditText inputPhone = ( EditText ) findViewById( R.id.input_form_phone );
        final EditText inputEmail = ( EditText ) findViewById( R.id.input_form_email );
        final RadioGroup inputGender = ( RadioGroup ) findViewById( R.id.input_form_gender );

        ContentValues pessoa = new ContentValues();
        pessoa.put( Pessoa.Contract.NOME, inputNome.getText().toString() );
        pessoa.put( Pessoa.Contract.TELEFONE, inputPhone.getText().toString() );
        pessoa.put( Pessoa.Contract.EMAIL, inputEmail.getText().toString() );
        pessoa.put( Pessoa.Contract.SEXO, "" + this.convertGenderToValue( inputGender ) );

        SQLiteDatabase db = Database.getInstance().getWritableDatabase();

        if ( this.pessoa == null || this.pessoa.getId() == 0 ) {
            db.insert( Pessoa.Contract.TABLE_NAME, null, pessoa );
        } else {
            db.update(
                Pessoa.Contract.TABLE_NAME,
                pessoa,
                Pessoa.Contract._ID + " = ?",
                new String[] { this.pessoa.getId() + "" }
            );
        }

        Toast.makeText( this, R.string.message_contact_saved, Toast.LENGTH_SHORT ).show();
        finish();

        return true;
    }

    protected final RadioButton convertGenderToRadio ( char gender ) {
        if ( gender == 'M' ) {
            return ( RadioButton ) findViewById( R.id.input_form_gender_male );
        } else {
            return ( RadioButton ) findViewById( R.id.input_form_gender_female );
        }
    }

    protected final char convertGenderToValue ( RadioGroup radioGroup ) {
        switch ( radioGroup.getCheckedRadioButtonId() ) {
            case R.id.input_form_gender_male:
                return 'M';

            case R.id.input_form_gender_female:
                return 'F';

            default:
                return 0;
        }
    }
}
