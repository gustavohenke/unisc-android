package br.unisc.aula8;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.unisc.aula8.db.Database;
import br.unisc.aula8.db.Pessoa;
import br.unisc.aula8.tasks.GetPessoaTask;
import br.unisc.aula8.tasks.SelectTask;

public class ViewActivity extends Activity {

    public static final String EXTRA_ID = "id";
    private int id;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_view );
    }

    @Override
    protected void onResume () {
        super.onResume();

        final Activity self = this;
        this.id = getIntent().getIntExtra( EXTRA_ID, 0 );

        new GetPessoaTask() {
            @Override
            protected void onPostExecute ( Pessoa pessoa ) {
                if ( pessoa == null ) {
                    Toast.makeText(
                        self,
                        R.string.message_contact_unknown,
                        Toast.LENGTH_SHORT
                    ).show();
                    self.finish();

                    return;
                }

                TextView textNome = ( TextView ) self.findViewById( R.id.view_nome );
                TextView textGender = ( TextView ) self.findViewById( R.id.view_gender );
                TextView textEmail = ( TextView ) self.findViewById( R.id.view_email );
                TextView textPhone = ( TextView ) self.findViewById( R.id.view_phone );

                textNome.setText( pessoa.getNome() );
                textGender.setText( getString( pessoa.getSexo() == 'M' ? R.string.form_gender_male : R.string.form_gender_female ) );
                textEmail.setText( pessoa.getEmail() );
                textPhone.setText( pessoa.getTelefone() );
            }
        }.execute( id );
    }

    @Override
    public boolean onCreateOptionsMenu ( Menu menu ) {
        getMenuInflater().inflate( R.menu.menu_view, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected ( MenuItem item ) {
        switch ( item.getItemId() ) {
            case R.id.menuitem_edit:
                Intent intent = new Intent( this, EditActivity.class );
                intent.putExtra( EditActivity.EXTRA_ID, this.id );
                startActivity( intent );
                break;

            case R.id.menuitem_delete:
                new AlertDialog.Builder( this )
                        .setMessage( R.string.message_delete )
                        .setPositiveButton( R.string.dialog_button_positive, null )
                        .setNegativeButton( R.string.dialog_button_negative, new DeleteContact() )
                        .show();
        }

        return super.onOptionsItemSelected( item );
    }

    private class DeleteContact implements DialogInterface.OnClickListener {

        @Override
        public void onClick ( DialogInterface dialog, int which ) {
            SQLiteDatabase db = Database.getInstance().getWritableDatabase();
            db.delete( Pessoa.Contract.TABLE_NAME, Pessoa.Contract._ID + " = ?", new String[] {
                id + ""
            });

            Toast.makeText( App.getContext(), R.string.message_deleted, Toast.LENGTH_SHORT ).show();
            finish();
        }
    }
}
