package br.unisc.aula8.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import br.unisc.aula8.App;

public final class Database extends SQLiteOpenHelper {

    private static Database instance;
    private final static String TYPE_INT = " INTEGER";
    private final static String TYPE_TEXT = " TEXT";
    private final static String TYPE_CHAR = " CHAR";

    private final static int DATABASE_VERSION = 1;
    private final static String DATABASE_NAME = "pessoa.db";

    private static final String SQL_CREATE_PESSOA =
            "CREATE TABLE " + Pessoa.Contract.TABLE_NAME + " (" +
                    Pessoa.Contract._ID + TYPE_INT + " PRIMARY KEY AUTOINCREMENT, " +
                    Pessoa.Contract.NOME + TYPE_TEXT + ", " +
                    Pessoa.Contract.EMAIL + TYPE_TEXT + ", " +
                    Pessoa.Contract.SEXO + TYPE_CHAR + ", " +
                    Pessoa.Contract.TELEFONE + TYPE_TEXT +
            ")";

    private static final String SQL_DELETE_PESSOA = "DROP TABLE IF EXISTS " + Pessoa.Contract.TABLE_NAME;

    private Database () {
        super( App.getContext(), DATABASE_NAME, null, DATABASE_VERSION );
    }

    public static Database getInstance () {
        if ( instance == null ) {
            instance = new Database();
        }

        return instance;
    }

    @Override
    public void onCreate ( SQLiteDatabase db ) {
        db.execSQL( SQL_CREATE_PESSOA );
    }

    @Override
    public void onUpgrade ( SQLiteDatabase db, int oldVersion, int newVersion ) {
        db.execSQL( SQL_DELETE_PESSOA );
        onCreate( db );
    }
}
