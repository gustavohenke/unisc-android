package br.unisc.aula8.db;

import android.provider.BaseColumns;

import br.unisc.aula8.tasks.SelectTask;

public class Pessoa {

    private int id;
    private String nome;
    private String telefone;
    private String email;
    private char sexo;

    public int getId () {
        return id;
    }

    public void setId ( int id ) {
        this.id = id;
    }

    public String getNome () {
        return nome;
    }

    public void setNome ( String nome ) {
        this.nome = nome;
    }

    public String getTelefone () {
        return telefone;
    }

    public void setTelefone ( String telefone ) {
        this.telefone = telefone;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail ( String email ) {
        this.email = email;
    }

    public char getSexo () {
        return sexo;
    }

    public void setSexo ( char sexo ) {
        this.sexo = sexo;
    }

    public static class Contract implements BaseColumns {

        public final static String TABLE_NAME = "PESSOA";
        public final static String NOME = "nome";
        public final static String EMAIL = "email";
        public final static String TELEFONE = "telefone";
        public final static String SEXO = "sexo";

    }
}