package br.unisc.aula8;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import br.unisc.aula8.db.Pessoa;
import br.unisc.aula8.tasks.GetPessoaTask;

public class EditActivity extends FormActivity {

    public static final String EXTRA_ID = "id";

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );

        final Context context = this;
        final int id = getIntent().getIntExtra( EditActivity.EXTRA_ID, 0 );

        new GetPessoaTask() {
            @Override
            protected void onPostExecute ( Pessoa pessoa ) {
                if ( pessoa == null ) {
                    Toast.makeText(
                        context,
                        R.string.message_contact_unknown,
                        Toast.LENGTH_SHORT
                    ).show();
                    finish();

                    return;
                }

                setPessoa( pessoa );
            }
        }.execute( id );
    }
}
