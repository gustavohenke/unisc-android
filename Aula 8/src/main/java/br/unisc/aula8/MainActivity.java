package br.unisc.aula8;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import br.unisc.aula8.adapters.PessoaAdapter;
import br.unisc.aula8.db.Pessoa;
import br.unisc.aula8.tasks.SelectTask;

public class MainActivity extends ListActivity {

    private PessoaAdapter adapter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        final Context context = this;
        findViewById( R.id.button_add ).setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick ( View v ) {
                Intent intent = new Intent( context, AddActivity.class );
                startActivity( intent );
            }
        } );

        this.adapter = new PessoaAdapter( this );
        this.setListAdapter( this.adapter );
    }

    @Override
    protected void onResume () {
        super.onResume();

        final View listView = getListView();
        final View messageEmptyView = findViewById( R.id.message_empty_list );

        new SelectTask( Pessoa.Contract.TABLE_NAME ) {
            @Override
            protected void onPostExecute ( Cursor cursor ) {
                super.onPostExecute( cursor );
                adapter.clear();

                boolean empty = cursor.getCount() == 0;
                if ( !empty ) {
                    while ( cursor.moveToNext() ) {
                        Pessoa pessoa = new Pessoa();
                        pessoa.setId( cursor.getInt( cursor.getColumnIndex( Pessoa.Contract._ID ) ) );
                        pessoa.setNome( cursor.getString( cursor.getColumnIndex( Pessoa.Contract.NOME ) ) );
                        adapter.add( pessoa );
                    }
                }

                adapter.notifyDataSetChanged();
                listView.setVisibility( empty ? View.GONE : View.VISIBLE );
                messageEmptyView.setVisibility( empty ? View.VISIBLE : View.GONE );

                cursor.close();
            }
        }.execute( new String[]{
            Pessoa.Contract._ID,
            Pessoa.Contract.NOME
        }, null, null, new String[]{
            Pessoa.Contract.NOME
        });
    }

    @Override
    protected void onListItemClick ( ListView l, View v, int position, long id ) {
        Pessoa pessoa = this.adapter.getItem( position );

        Intent intent = new Intent( this, ViewActivity.class );
        intent.putExtra( ViewActivity.EXTRA_ID, pessoa.getId() );
        startActivity( intent );
    }
}
