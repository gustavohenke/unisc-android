package br.unisc.aula8.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import br.unisc.aula8.R;
import br.unisc.aula8.db.Pessoa;

public class PessoaAdapter extends ArrayAdapter< Pessoa > {

    public PessoaAdapter ( Context context ) {
        super( context, 0 );
    }

    @Override
    public View getView ( int position, View convertView, ViewGroup parent ) {
        Pessoa pessoa = getItem( position );

        if ( convertView == null ) {
            convertView = LayoutInflater.from( this.getContext() )
                                        .inflate( R.layout.listitem_pessoa, parent, false );
        }

        TextView textNome = ( TextView ) convertView.findViewById( R.id.listitem_pessoa_nome );
        textNome.setText( pessoa.getNome() );

        return convertView;
    }
}
