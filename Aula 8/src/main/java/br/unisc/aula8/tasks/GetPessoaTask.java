package br.unisc.aula8.tasks;

import android.database.Cursor;
import android.os.AsyncTask;

import br.unisc.aula8.db.Pessoa;

public class GetPessoaTask extends AsyncTask< Integer, Void, Pessoa > {
    @Override
    protected Pessoa doInBackground ( Integer... params ) {
        int id = params[ 0 ];
        Cursor cursor = new SelectTask( Pessoa.Contract.TABLE_NAME ) {}.doInBackground( new String[]{
                Pessoa.Contract.NOME,
                Pessoa.Contract.SEXO,
                Pessoa.Contract.EMAIL,
                Pessoa.Contract.TELEFONE
        }, new String[]{
                Pessoa.Contract._ID + " = ?"
        }, new String[]{
                id + ""
        });

        if ( cursor.getCount() == 0 ) {
            return null;
        }

        cursor.moveToFirst();

        Pessoa pessoa = new Pessoa();
        pessoa.setId( id );
        pessoa.setNome( cursor.getString( cursor.getColumnIndex( Pessoa.Contract.NOME ) ) );
        pessoa.setTelefone( cursor.getString( cursor.getColumnIndex( Pessoa.Contract.TELEFONE ) ) );
        pessoa.setEmail( cursor.getString( cursor.getColumnIndex( Pessoa.Contract.EMAIL ) ) );
        pessoa.setSexo( cursor.getString( cursor.getColumnIndex( Pessoa.Contract.SEXO ) ).charAt( 0 ) );

        return pessoa;
    }

}
