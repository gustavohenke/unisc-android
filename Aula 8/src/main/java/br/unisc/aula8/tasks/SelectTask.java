package br.unisc.aula8.tasks;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import br.unisc.aula8.db.Database;

public abstract class SelectTask extends AsyncTask< String[], Void, Cursor > {

    private final String table;

    public SelectTask ( String table ) {
        this.table = table;
    }

    @Override
    protected Cursor doInBackground ( String[]... params ) {
        SQLiteDatabase db = Database.getInstance().getReadableDatabase();
        final String[] projection = params[ 0 ];
        final String selection = params.length > 1 ? joinArray( params[ 1 ], " AND " ) : null;
        final String[] selectionArgs = params.length > 2 ? params[ 2 ] : null;
        final String order = params.length > 3 ? joinArray( params[ 3 ], ", " ) : null;

        Log.d( "select", selection == null ? "" : selection );
        Log.d( "select", joinArray( selectionArgs == null ? new String[] {} : selectionArgs, ", " ) );

        return db.query( this.table, projection, selection, selectionArgs, null, null, order );
    }

    private static String joinArray ( String[] arr, String glue ) {
        if ( arr == null ) {
            return null;
        }

        String ret = "";
        for ( String item : arr ) {
            ret += item + glue;
        }
        return ret.substring( 0, Math.max( 0, ret.length() - glue.length() ) );
    }
}
