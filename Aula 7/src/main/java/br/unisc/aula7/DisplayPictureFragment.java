package br.unisc.aula7;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

public class DisplayPictureFragment extends Fragment {

    public static final String IMAGE_URI = "imageUri";
    private Context context;

    public static DisplayPictureFragment newInstance ( Uri imageUri ) {
        Bundle args = new Bundle();
        args.putParcelable( IMAGE_URI, imageUri );

        DisplayPictureFragment fragment = new DisplayPictureFragment();
        fragment.setArguments( args );
        return fragment;
    }

    public DisplayPictureFragment () {
        // Required empty public constructor
    }

    @Override
    public View onCreateView ( LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState ) {
        return inflater.inflate( R.layout.fragment_display_picture, container, false );
    }

    @Override
    public void onViewCreated ( View view, Bundle savedInstanceState ) {
        super.onViewCreated( view, savedInstanceState );

        Bundle args = getArguments();
        if ( args == null ) {
            return;
        }

        Context context = getActivity();
        Uri imageUri = args.getParcelable( IMAGE_URI );
        ContentResolver resolver = context.getContentResolver();
        resolver.notifyChange( imageUri, null );

        try {
            Bitmap bitmap = BitmapFactory.decodeFile( imageUri.getPath() );
            ImageView picture = ( ImageView ) getView().findViewById( R.id.image_picture );
            picture.setImageBitmap( bitmap );
        } catch ( Exception e ) {
            e.printStackTrace();
            Toast.makeText( context, "Erro ao carregar a imagem", Toast.LENGTH_SHORT ).show();
        }
    }
}
