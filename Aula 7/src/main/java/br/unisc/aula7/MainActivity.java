package br.unisc.aula7;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class MainActivity extends Activity {

    private static final int TAKE_PICTURE = 1;
    private static final int SEND_EMAIL = 2;
    private Uri imageUri;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
    }

    public void takePhoto ( View view ) throws IOException {
        File photo = File.createTempFile( "picture", ".jpg", Environment.getExternalStorageDirectory() );
        this.imageUri = Uri.fromFile( photo );

        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
        intent.putExtra( MediaStore.EXTRA_OUTPUT, this.imageUri );

        startActivityForResult( intent, TAKE_PICTURE );
    }

    public void sendEmail ( View view ) {
        if ( this.imageUri == null ) {
            Toast.makeText( this, "Você não tirou uma foto ainda.", Toast.LENGTH_SHORT ).show();
            return;
        }

        Intent intent = new Intent( Intent.ACTION_SEND );
        intent.setType( "text/plain" );
        intent.putExtra( Intent.EXTRA_STREAM, this.imageUri );
        startActivity( Intent.createChooser( intent, "Enviar e-mail" ) );
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data ) {
        super.onActivityResult( requestCode, resultCode, data );

        switch ( requestCode ) {
            case TAKE_PICTURE:
                if ( resultCode == Activity.RESULT_OK ) {
                    Fragment fragment = DisplayPictureFragment.newInstance( this.imageUri );
                    getFragmentManager()
                            .beginTransaction()
                            .replace( R.id.fragment_container, fragment )
                            .commit();
                }
                break;
        }
    }
}
