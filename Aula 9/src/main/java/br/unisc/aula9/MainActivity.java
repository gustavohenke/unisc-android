package br.unisc.aula9;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONObject;

import java.io.IOException;

public class MainActivity extends Activity {

    private final static MediaType JSON = MediaType.parse( "application/json" );
    private final static String PAPAGAIO_URL = "https://papagaio.ml/api";
    private final OkHttpClient client = new OkHttpClient();
    private Button submit;
    private EditText username;
    private EditText email;
    private EditText password;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        username = ( EditText ) findViewById( R.id.username );
        email = ( EditText ) findViewById( R.id.email );
        password = ( EditText ) findViewById( R.id.password );

        username.setOnKeyListener( new ToggleSubmit() );
        email.setOnKeyListener( new ToggleSubmit() );
        password.setOnKeyListener( new ToggleSubmit() );

        submit = ( Button ) findViewById( R.id.submit );
        submit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick ( View v ) {
                submit.setEnabled( false );
                createAccount();
            }
        } );
    }

    public void createAccount () {
        new AsyncTask< String, Void, Response >() {
            private Exception exception;

            @Override
            protected Response doInBackground ( String... params ) {
                final JSONObject json = new JSONObject();

                try {
                    json.put( "username", params[ 0 ] );
                    json.put( "email", params[ 1 ] );
                    json.put( "password", params[ 2 ] );
                } catch ( Exception e ) {
                    exception = e;
                    return null;
                }

                Log.d( "foo", json.toString() );
                RequestBody body = RequestBody.create( JSON, json.toString() );

                Request request = new Request.Builder()
                        .url( PAPAGAIO_URL + "/user" )
                        .post( body )
                        .build();

                try {
                    return client.newCall( request ).execute();
                } catch ( IOException e ) {
                    exception = e;
                    return null;
                }
            }

            @Override
            protected void onPostExecute ( Response response ) {
                super.onPostExecute( response );
                submit.setEnabled( true );

                if ( exception != null ) {
                    showDialog( "Erro", exception.getMessage() );
                    return;
                }

                try {
                    JSONObject json = new JSONObject( response.body().string() );

                    if ( response.code() >= 400 ) {
                        showDialog(
                                json.optString( "err" ),
                                json.optString( "message" )
                        );
                        return;
                    }

                    showDialog(
                            "Usuário criado",
                            "Você pode fazer login com o usuário " + json.getString( "username" )
                    );
                } catch ( Exception e ) {
                    e.printStackTrace();
                }
            }
        }.execute(
                username.getText().toString(),
                email.getText().toString(),
                password.getText().toString()
        );
    }

    private void showDialog ( String title, String message ) {
        new AlertDialog.Builder( this )
                .setTitle( title )
                .setMessage( message )
                .show();
    }

    class ToggleSubmit implements View.OnKeyListener {

        @Override
        public boolean onKey ( View v, int keyCode, KeyEvent event ) {
            if ( username.getText().length() > 0
                    && email.getText().length() > 0
                    && password.getText().length() > 0 ) {
                submit.setEnabled( true );
            } else {
                submit.setEnabled( false );
            }

            return false;
        }
    }
}
