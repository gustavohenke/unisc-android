package br.unisc.aula13;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener {

    private static final float MAX_LIGHT = 1000;
    private SensorManager sensorManager;
    private Sensor light;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        sensorManager = ( SensorManager ) getSystemService( SENSOR_SERVICE );
        light = sensorManager.getDefaultSensor( Sensor.TYPE_LIGHT );

        findViewById( R.id.play ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick ( View view ) {
                Intent intent = new Intent( Broadcasts.PLAY );
                sendBroadcast( intent );
            }
        } );

        findViewById( R.id.stop ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick ( View v ) {
                Intent intent = new Intent( Broadcasts.STOP );
                sendBroadcast( intent );
            }
        } );
    }

    @Override
    protected void onStart () {
        super.onStart();
        sensorManager.registerListener( this, light, SensorManager.SENSOR_DELAY_NORMAL );
    }

    @Override
    protected void onStop () {
        super.onStop();
        sensorManager.unregisterListener( this );
    }

    @Override
    public void onSensorChanged ( SensorEvent event ) {
        float luminosidade = event.values[ 0 ];
        TextView textLuminosidade = ( TextView ) findViewById( R.id.luminosidade );
        textLuminosidade.setText( String.valueOf( luminosidade ) );

        // Ou minha casa (onde desenvolvi e testei) é meio escura, ou meu sensor de luz é ruim.
        // Por isso limitei o volume máximo, definido na constante, pra ter resultados melhores.
        float maxRange = Math.min( light.getMaximumRange(), MAX_LIGHT );
        float lastRead = Math.min( luminosidade, MAX_LIGHT );
        float log = ( float ) ( Math.log( maxRange - lastRead ) / Math.log( maxRange ) );
        float vol = 1 - log;

        Intent volIntent = new Intent( Broadcasts.VOLUME );
        volIntent.putExtra( RadioService.VolumeReceiver.EXTRA_VOLUME, vol );
        sendBroadcast( volIntent );

        TextView textVol = ( TextView ) findViewById( R.id.volume );
        textVol.setText( String.valueOf( vol ) );
    }

    @Override
    public void onAccuracyChanged ( Sensor sensor, int accuracy ) {

    }
}
