package br.unisc.aula13;

public interface Broadcasts {

    String PLAY = "aula13.play";
    String STOP = "aula13.stop";
    String VOLUME = "aula13.volume";

}
