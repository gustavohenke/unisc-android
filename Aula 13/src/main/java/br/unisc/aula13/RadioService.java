package br.unisc.aula13;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.widget.Toast;

public class RadioService extends Service {
    private static final String URL = "http://42747t.lp.azioncdn.net/2747t/a/mp4:access_options/rtmp-live/atl_poa.sdp/playlist.m3u8";
    private static int ID = 1;
    private MediaPlayer player;
    private BroadcastReceiver volReceiver;

    public RadioService () {}

    @Override
    public IBinder onBind ( Intent intent ) {
        throw new UnsupportedOperationException( "Not yet implemented" );
    }

    @Override
    public void onCreate () {
        super.onCreate();

        volReceiver = new VolumeReceiver();
        registerReceiver( volReceiver, new IntentFilter( Broadcasts.VOLUME ) );

        Intent playIntent = new Intent( Broadcasts.PLAY );
        PendingIntent playPendingIntent = PendingIntent.getBroadcast( this, 0, playIntent, 0 );

        Intent stopIntent = new Intent( Broadcasts.STOP );
        PendingIntent stopPendingIntent = PendingIntent.getBroadcast( this, 0, stopIntent, 0 );

        Notification.Action playAction = new Notification.Action(
                R.drawable.ic_play_arrow_24dp,
                "Play",
                playPendingIntent
        );

        Notification.Action stopAction = new Notification.Action(
                R.drawable.ic_stop_24dp,
                "Stop",
                stopPendingIntent
        );

        Intent activityIntent = new Intent( this, MainActivity.class );
        PendingIntent activityPendingIntent = PendingIntent.getActivity( this, 0, activityIntent, 0 );

        Notification notification = new Notification.Builder( this )
                .setSmallIcon( R.drawable.ic_my_library_music_24dp )
                .setContentTitle( "Aula13 Player" )
                .setContentText( "Agora escutando a Rádio Atlântida Online" )
                .setOngoing( true )
                .setContentIntent( activityPendingIntent )
                .addAction( playAction )
                .addAction( stopAction )
                .build();

        NotificationManager notificationMgr = ( NotificationManager ) getSystemService( Context.NOTIFICATION_SERVICE );
        notificationMgr.notify( ID, notification );
    }

    @Override
    public int onStartCommand ( Intent intent, int flags, int startId ) {
        if ( player == null ) {
            AudioThread thread = new AudioThread();
            thread.start();
        } else {
            player.start();
        }

        return super.onStartCommand( intent, flags, startId );
    }

    @Override
    public void onDestroy () {
        super.onDestroy();

        if ( player.isPlaying() ) {
            player.stop();
        }

        player.release();
        player = null;

        unregisterReceiver( volReceiver );
    }

    // ---------------------------------------------------------------------------------------------

    private void initPlayer ( MediaPlayer.OnPreparedListener listener ) {
        player = new MediaPlayer();
        player.setAudioStreamType( AudioManager.STREAM_MUSIC );
        player.setOnPreparedListener( listener );

        try {
            player.setDataSource( URL );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    // ---------------------------------------------------------------------------------------------

    class AudioThread extends Thread implements MediaPlayer.OnPreparedListener {

        @Override
        public void run () {
            initPlayer( this );
            player.prepareAsync();
        }

        @Override
        public void onPrepared ( MediaPlayer player ) {
            player.start();
        }
    }

    // ---------------------------------------------------------------------------------------------

    public class VolumeReceiver extends BroadcastReceiver {

        public static final String EXTRA_VOLUME = "volume";

        @Override
        public void onReceive ( Context context, Intent intent ) {
            float vol = intent.getFloatExtra( VolumeReceiver.EXTRA_VOLUME, 0.5f );
            player.setVolume( vol, vol );
        }
    }

    public static class PlayReceiver extends BroadcastReceiver {

        @Override
        public void onReceive ( Context context, Intent intent ) {
            Intent serviceIntent = new Intent( context, RadioService.class );
            context.startService( serviceIntent );
        }
    }

    public static class StopReceiver extends BroadcastReceiver {

        @Override
        public void onReceive ( Context context, Intent intent ) {
            Intent serviceIntent = new Intent( context, RadioService.class );
            boolean stopped = context.stopService( serviceIntent );

            if ( stopped ) {
                Toast.makeText(
                        context,
                        "Clique em Stop novamente para dispensar a notificação",
                        Toast.LENGTH_LONG
                ).show();
            } else {
                NotificationManager notificationMgr = ( NotificationManager ) context.getSystemService( Context.NOTIFICATION_SERVICE );
                notificationMgr.cancel( ID );
            }
        }
    }
}
