package br.unisc.prova2014.receiver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import br.unisc.prova2014.MainActivity;

public class DismissNotificationReceiver extends BroadcastReceiver {
    public DismissNotificationReceiver () {}

    @Override
    public void onReceive ( Context context, Intent intent ) {
        NotificationManager notificationMgr = ( NotificationManager ) context.getSystemService( Context.NOTIFICATION_SERVICE );
        notificationMgr.cancel( SmsReceiver.ID_NOTIFICATION );

        Intent activityIntent = new Intent( context, MainActivity.class );
        context.startActivity( activityIntent );
    }
}
