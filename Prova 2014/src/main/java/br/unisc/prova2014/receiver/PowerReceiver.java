package br.unisc.prova2014.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

public class PowerReceiver extends BroadcastReceiver {
    public PowerReceiver () {
    }

    @Override
    public void onReceive ( Context context, Intent intent ) {
        int status = intent.getIntExtra( BatteryManager.EXTRA_STATUS, -1 );
        if ( status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL ) {
            String tipo = "desconhecido";
            int plug = intent.getIntExtra( BatteryManager.EXTRA_PLUGGED, -1 );

            if ( plug == BatteryManager.BATTERY_PLUGGED_USB ) {
                tipo = "USB";
            } else if ( plug == BatteryManager.BATTERY_PLUGGED_AC ) {
                tipo = "AC";
            } else if ( plug == BatteryManager.BATTERY_PLUGGED_WIRELESS ) {
                tipo = "wireless";
            }

            Log.i( "POWER", "Bateria carregando via " + tipo );
            Toast.makeText( context, "Bateria carregando via " + tipo, Toast.LENGTH_LONG ).show();
        } else {
            Log.i( "POWER", "Bateria descarregando" );
            Toast.makeText( context, "Bateria descarregando", Toast.LENGTH_LONG ).show();
        }


    }
}
