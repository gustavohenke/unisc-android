package br.unisc.prova2014.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import br.unisc.prova2014.MainActivity;
import br.unisc.prova2014.R;

public class SmsReceiver extends BroadcastReceiver {
    public final static int ID_NOTIFICATION = 1;
    public final static String BROADCAST_DISMISS = "prova2014.dismiss";

    public SmsReceiver () {
    }

    @Override
    public void onReceive ( Context context, Intent intent ) {
        SmsMessage[] SMSs = null;
        Bundle extras = intent.getExtras();
        if ( extras != null ) {
            Object[] pdus = ( Object[] ) extras.get( "pdus" );
            SMSs = new SmsMessage[ pdus.length ];
            for ( int i = 0; i < pdus.length; i++ ) {
                //noinspection deprecation
                SMSs[ i ] = SmsMessage.createFromPdu( ( byte[] ) pdus[ i ] );
            }
        }

        if ( SMSs == null || SMSs.length == 0 ) {
            return;
        }

        Toast.makeText( context, "SMS Recebido", Toast.LENGTH_LONG ).show();

        Intent dismissIntent = new Intent( BROADCAST_DISMISS );
        PendingIntent dismissPendingIntent = PendingIntent.getBroadcast( context, 0, dismissIntent, 0 );

        NotificationManager notificationMgr = ( NotificationManager ) context.getSystemService( Context.NOTIFICATION_SERVICE );
        Notification notification = new Notification.Builder( context )
                .setSmallIcon( R.mipmap.ic_launcher )
                .setContentTitle( "SMS Recebido" )
                .setContentText( "Toque para exibir todos SMSs recebidos" )
                .setContentIntent( dismissPendingIntent )
                .build();

        notificationMgr.notify( ID_NOTIFICATION, notification );
    }
}
