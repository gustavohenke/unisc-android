package br.unisc.prova2014.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Este receiver executa com o intent-filter android.intent.action.PHONE_STATE
 * e requer a permissão android.permission.READ_PHONE_STATE!
 */
public class PhoneReceiver extends BroadcastReceiver {
    public PhoneReceiver () {}

    @Override
    public void onReceive ( Context context, Intent intent ) {
        String msg;
        String state = intent.getStringExtra( TelephonyManager.EXTRA_STATE );
        if ( state.equals( TelephonyManager.EXTRA_STATE_IDLE ) ) {
            // Fazer algo com o telefone ocioso
            msg = "Telefone ocioso";
        } else if ( state.equals( TelephonyManager.EXTRA_STATE_OFFHOOK ) ) {
            // Fazer algo se o telefone estiver "fora do gancho":
            // Ligação discando, em andamento, ou aguardando.
            msg = "Telefone fora do gancho";
        } else {
            // Fazer algo se o telefone estiver tocando
            msg = "Recebendo ligação: " + intent.getStringExtra( TelephonyManager.EXTRA_INCOMING_NUMBER );
        }

        Log.i( "PHONE", msg );
        Toast.makeText( context, msg, Toast.LENGTH_LONG ).show();
    }
}
