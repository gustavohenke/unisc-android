package br.unisc.prova2014.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;
import android.util.Log;
import android.widget.Toast;

public class BatteryReceiver extends BroadcastReceiver {
    public BatteryReceiver () {
    }

    @Override
    public void onReceive ( Context context, Intent intent ) {
        int level = intent.getIntExtra( BatteryManager.EXTRA_LEVEL, -1 );
        int scale = intent.getIntExtra( BatteryManager.EXTRA_SCALE, -1 );

        float battery = level / ( float ) scale;
        Log.i( "BATTERY", "Nível: " + battery + "%" );
        Toast.makeText( context, "Nível da bateria alterado: " + battery + "%", Toast.LENGTH_LONG ).show();
    }
}
